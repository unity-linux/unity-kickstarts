# unity-common-packages.ks
# 
# Description:
# - Unity-Linux Common packages that should be installed or removed across all environments
#
# Maintainer(s):
# - JMiahMan@unity-linux.org

%packages --ignoremissing
# Branding
breeze-cursor-theme
unity-extras
unity-release
unity-logos
unity-welcome
numix-icon-theme-circle
anaconda-extras-unity

#unity-bookmarks
#-fedora-bookmarks
-fedora-release-notes
#plymouth-theme-unity
grub2-efi-ia32
grub2-efi-x64
efibootmgr
#unity-icon-theme-base
#unity-icon-theme
-unity-release-workstation
-fedora-release-workstation
-f22-backgrounds*

# Unity Linux Tools
golivecd
upaste

# Internet
-firefox
chromium
chromium-libs-media-freeworld

# Multimedia
HandBrake-gui
audacity-freeworld
dvb-apps
ffmpeg
ffmpegthumbnailer
#flash-plugin (Add this later please)
gstreamer-ffmpeg
gstreamer-plugins-bad
gstreamer-plugins-bad-free-extras
gstreamer-plugins-good
gstreamer-plugins-good-extras
gstreamer-plugins-ugly
gstreamer1-plugins-bad-free
gstreamer1-plugins-bad-free-extras
gstreamer1-plugins-good
gstreamer1-plugins-good-extras
gstreamer1-plugins-ugly
libaacs
raw-thumbnailer
vlc-extras

%end
