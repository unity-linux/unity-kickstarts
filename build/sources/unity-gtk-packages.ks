# unity-gtk-packages.ks
# 
# Description:
# - Unity-Linux gtk packages that should be installed or removed across all gtk based ISOs
#
# Maintainer(s):
# - JMiahMan@unity-linux.org

%packages --ignoremissing

## --- Additions --- ##
#Graphics
gimp

#Internet
hexchat

#Office
evince
cura-lulzbot
libreoffice
libreoffice-gtk3
simple-scan

#Multimedia
pithos

## --- Removals --- ##
#Office
-abiword
-gnumeric

%end