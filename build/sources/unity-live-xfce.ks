# unity-livecd-xfce.ks
#
# Description:
# - Unity-Linux kickstart for the light-weight XFCE Desktop Environment
#
# Maintainer(s):
# - JMiahMan    <jmiahman@unity-linux.org>

#This iclude must be first of the kickstart won't flatten properly
%include unity-live-base.ks

%post
echo -e "\n*****\nXFCE POST SECTION\n*****\n"

# fix partially (32Bit lib needed as well) gtk3-nocsd error
ARCH=$(rpm -q elfutils-libs --qf "%{arch}")
if [ "$ARCH" == "x86_64" ]; then
cat >> /etc/skel/.bashrc << EOF
export LD_PRELOAD=/usr/lib64/libgtk3-nocsd.so.0
EOF

elif [ "$ARCH" == "i686" ]; then
cat >> /etc/skel/.bashrc << EOF
export LD_PRELOAD=/usr/lib/libgtk3-nocsd.so.0
EOF
fi

#Optimize DNF
if ( rpm -V deltarpm ); then
cat >> /etc/dnf/dnf.conf << EOF
fastestmirror=true
deltarpm=true
EOF
fi

# xfce configuration
cat > /.buildstamp << EOF
[Main]
Product=Unity-Linux
Variant=Workstation
IsFinal=True
EOF

# create /etc/sysconfig/desktop (needed for installation)
cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/startxfce4
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

cat > /etc/lightdm/slick-greeter.conf << EOF
[Greeter]
background=/usr/share/backgrounds/unity/default/standard/unity.png
draw-grid=false
theme-name=Arc-Dark
show-power=true
show-keyboard=false
activate-numlock=true
EOF

cat >> /etc/rc.d/init.d/livesys << EOF

restorecon -R / || :

mkdir -p /home/liveuser/.config/xfce4

cat > /home/liveuser/.config/xfce4/helpers.rc << FOE
MailReader=sylpheed-claws
FileManager=Thunar
WebBrowser=firefox
FOE

# disable screensaver locking (#674410)
cat >> /home/liveuser/.xscreensaver << FOE
mode:           off
lock:           False
dpmsEnabled:    False
FOE

# deactivate xfconf-migration (#683161)
rm -f /etc/xdg/autostart/xfconf-migration-4.6.desktop || :

# deactivate xfce4-panel first-run dialog (#693569)
mkdir -p /home/liveuser/.config/xfce4/xfconf/xfce-perchannel-xml
#cp /etc/xdg/xfce4/panel/default.xml /home/liveuser/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
#sed -i 's/^#show-language-selector=.*/show-language-selector=true/' /etc/lightdm/lightdm-gtk-greeter.conf

# set Xfce as default session, otherwise login will fail
sed -i 's/^#user-session=.*/user-session=xfce/' /etc/lightdm/lightdm.conf

# Show harddisk install on the desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
mkdir /home/liveuser/Desktop
cp /usr/share/applications/liveinst.desktop /home/liveuser/Desktop
sed -i 's!Exec=.*!Exec=sudo /sbin/anaconda --liveinst --method=livecd:/dev/loop2!g' /home/liveuser/Desktop/liveinst.desktop
chmod +x /home/liveuser/Desktop/liveinst.desktop

# no updater applet in live environment
rm -f /etc/xdg/autostart/org.mageia.dnfdragora-updater.desktop

# and mark it as executable (new Xfce security feature)
chmod +x /home/liveuser/Desktop/liveinst.desktop

# KP - re'sync the /etc/skel settings for xfce and 
setsebool -P rsync_full_access 1
rsync -Pa /etc/xdg/xfce4 /home/liveuser/.config
rsync -Pa /etc/skel/.config/xfce4 /home/liveuser/.config

# this goes at the end after all other changes. 
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home

EOF

%end

# These includes must be here or the kickstart won't flatten properly
%include unity-live-minimization.ks
%include unity-xfce-common.ks
%include unity-common-packages.ks
%include unity-gtk-packages.ks
