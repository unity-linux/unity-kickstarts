# Include the appropriate repo definitions

# Exactly one of the following should be uncommented

# For the master branch the following should be uncommented
# %include unity-repo-testing.ks

# For non-master branches the following should be uncommented
 %include unity-repo-not-testing.ks
