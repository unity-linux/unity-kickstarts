# unity-livecd-xfce.ks
#
# Description:
# - Unity-Linux kickstart for the light-weight XFCE Desktop Environment
#
# Maintainer(s):
# - JMiahMan    <jmiahman@unity-linux.org>

%packages --ignoremissing

@networkmanager-submodules
@xfce-desktop
@xfce-apps
@xfce-extra-plugins
@xfce-media
@xfce-office

xfce4-whiskermenu-plugin

# Use slick & light-locker
slick-greeter
light-locker

#Branding Packages
-xfce4-settings
-xfce4-session-engines
unity-settings-xfce
unity-backgrounds-xfce
unity-backgrounds-extras-xfce
gtk3-nocsd
gtk3-nocsd-lib
#32Bit Lib needed
gtk3-nocsd-lib.i686

# Unity Specific Desktop additions
claws-mail
vlc

# unlock default keyring. FIXME: Should probably be done in comps
gnome-keyring-pam
# Admin tools are handy to have
@admin-tools
wget
xfsprogs

# Better more popular browser
#firefox
system-config-printer

# save some space
-autofs
-acpid
-gimp-help
-desktop-backgrounds-basic
-aspell-*                   # dictionaries are big
-xfce4-sensors-plugin
-xscreensaver-base
-xscreensaver-extras-base
-xscreensaver-gl-base
-xscreensaver-gl-extras

%end
