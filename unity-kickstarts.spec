Name:       unity-kickstarts
Version:    0.30.0
Release:    1%{?dist}
License:    GPLv2+
Summary:    Kickstart files and templates for creating your own Unity-Linux isos 
Group:      Applications/System
URL:        https://www.unity-linux.org
Source0:    %{name}-%{version}.tar.xz
BuildArch:  noarch
BuildRequires:	git

%description
A number of kickstarts you can use to create Unity-Linux isos

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT/usr

%files
#%doc COPYING README.md AUTHORS
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/*.ks*
#%{_datadir}/%{name}/snippets/

%changelog
* Fri Apr 26 2019 JMiahMan <jmiahman@unity-linux.org> - 0.30.0-1
- Bump for 30

* Thu Apr 04 2019 JMiahMan <jmiahman@unity-linux.org> - 0.29.0-3
- Ignore missing packages in common

* Thu Oct 18 2018 JMiahMan <jmiahman@unity-linux.org> - 0.29.0-1
- Initial creation
